//
//  Cash.swift
//  CumCum
//
//  Created by net=0 on 10/11/20.
//

import Foundation

enum CurrencyUni: Int {
    case USD = 23270,
            EUR = 27663,
            KWD = 78000
    
}

class Cash {
    var currencyUnit: CurrencyUni = .USD // stored property
    var value = 0
    
    // Chứa nghiệp vụ
    var valueInVND: Int { // computed property
        return value * currencyUnit.rawValue
    }
    
}
