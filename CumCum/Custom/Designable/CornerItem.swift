//
//  CornerButton.swift
//  NetworkingTeam
//
//  Created by net=0 on 9/10/20.
//

import UIKit

@IBDesignable
class CornerItem: UIButton{
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet{
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
}
