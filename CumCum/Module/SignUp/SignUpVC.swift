//
//  SignUpVC.swift
//  TEAM1
//
//  Created by net=0 on 9/10/20.
//

import UIKit
import Parse

class SignUpVC: UIViewController {
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldRePassword: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var textFieldBio: UITextField!
    @IBOutlet weak var textFieldWeb: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSubmit: CornerItem!
    @IBOutlet weak var btnCancel: CornerItem!
    
    
    var scrollViewHeight: CGFloat = 0
    var keyboard = CGRect()
    let user = PFUser()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register"
        self.navigationController?.isNavigationBarHidden = false
        
        configScrollView()
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
        imgAvatar.clipsToBounds = true
        
        hideKeyboardWhenTap()
        selectImageForAvartar()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
//        checkUserIsValid()
        
    }
    
    func configScrollView() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        scrollView.contentSize.height = self.view.frame.height
        scrollViewHeight = scrollView.frame.size.height
        // When input text can scroll down or up
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    func selectImageForAvartar() {
        let avatarTap = UITapGestureRecognizer(target: self, action: #selector(loadImg))
        avatarTap.numberOfTapsRequired = 1
        imgAvatar.isUserInteractionEnabled = true
        imgAvatar.addGestureRecognizer(avatarTap)
    }
    
    func hideKeyboardWhenTap(){
        
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        view.addGestureRecognizer(hideTap)
        
    }
    
    
    
    // scroll down, up to enter infor
    @objc func showKeyboard(notification: Notification){
        // define keyboard size
        keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue!
        
        // move up UI
        UIView.animate(withDuration: 0.4) {
            self.scrollView.frame.size.height = self.scrollViewHeight - self.keyboard.height
        }
    }
    
    @objc func hideKeyboard(){
        
        // move down UI
        UIView.animate(withDuration: 0.4) {
            self.scrollView.frame.size.height = self.view.frame.height
        }
    }

    // sign up / submit pressed
    @IBAction func submitAction(_ sender: Any) {
        dismissKeyboard()
        if textFieldUsername.text!.isEmpty || textFieldPassword.text!.isEmpty || textFieldRePassword.text!.isEmpty || textFieldEmail.text!.isEmpty || textFieldFullname.text!.isEmpty || textFieldBio.text!.isEmpty || textFieldWeb.text!.isEmpty {
            Common.displayAlert(title: "PLEASE", message: "fill all fields", vc: self)
        }
        
        if textFieldPassword.text != textFieldRePassword.text {
            Common.displayAlert(title: "PASSWORD", message: "do not match", vc: self)
        }
        sendDataToServer()
    }
    
    // send all data to server
    func sendDataToServer() {
        let now = NSDate()
        let nowTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now)
        let dateToday = nowTimeStamp
        let usernameInput = textFieldUsername.text?.lowercased()
        user.username = usernameInput
        user.email = textFieldEmail.text?.lowercased()
        user.password = textFieldPassword.text
        user["fullname"] = textFieldFullname.text
        user["bio"] = textFieldBio.text
        user["web"] = textFieldWeb.text?.lowercased()
        
        // change in profile vc
        user["tel"] = ""
        user["gender"] = ""
        
        // convert our image for sending to server
        let avaData = imgAvatar.image?.jpegData(compressionQuality: 0.5)
        let avaFile = PFFileObject(name: usernameInput! + "_" + dateToday + "_avatar.jpg", data: avaData!)
        user["ava"] = avaFile
        
        // save data in server
        user.signUpInBackground { (success, error) in
            if error == nil {
                // remember logged user
                UserDefaults.standard.set(self.user.username, forKey: "username")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.login()
                
            } else {
                let systemErr = error!.localizedDescription
                Common.displayAlert(title: "ERROR!!!", message: systemErr, vc: self)
            }
        }
        
        
    }
    
    fileprivate func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd"
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
    
    
    @IBAction func clearAction(_ sender: Any) {
        // Reset form
        let defaultImage = UIImage(named: "pp")
        imgAvatar.image = defaultImage
        textFieldUsername.text = ""
        textFieldPassword.text = ""
        textFieldRePassword.text = ""
        textFieldEmail.text = ""
        textFieldFullname.text = ""
        textFieldBio.text = ""
        textFieldWeb.text = ""
        
    }
    
    // hide keyboard when tap 
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}

extension SignUpVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // load image when tap the icon avatar
    @objc func loadImg(recognizer: UITapGestureRecognizer){
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    // connect selected image to our ImageView
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imgAvatar.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
