//
//  GuestVC.swift
//  TEAM1
//
//  Created by net=0 on 17/10/20.
//

import UIKit
import Parse

var guestName = [String]()

class GuestVC: UIViewController {
    
    var collectionView: UICollectionView!

    // UI objects
    var refresher: UIRefreshControl!
    var page: Int = 10
    
    // storing data from server
    var uuidArray = [String]()
    var picArray = [PFFileObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set guest name in the top
        self.navigationItem.title = guestName.last!.uppercased()
        self.navigationController?.navigationBar.barTintColor = .white
        
        // new back button
        backToUser()
        swipeToBack()
        
        configCollectionView()
        pullToRefresh()
        
        // call load post
        loadPosts()
        

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.frame = view.bounds
    }
    
    fileprivate func backToUser() {
        // new back button
        self.navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(title: "back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = backBtn
    }
    
    fileprivate func swipeToBack() {
        // swipe to go back
        
        let backSwipe = UISwipeGestureRecognizer(target: self, action: #selector(backAction))
        backSwipe.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(backSwipe)
    }
    
    fileprivate func pullToRefresh() {
        // pull to refresh
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshAction), for: UIControl.Event.valueChanged)
        collectionView?.addSubview(refresher)
    }
    
    
    
    @objc func refreshAction() {
        collectionView.reloadData()
        refresher.endRefreshing()
    }
    
    // posts loading
    
    func loadPosts() {
        
        // load posts
        let query = PFQuery(className: "posts")
        query.whereKey("username", equalTo: guestName.last!)
        query.limit = page
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // find relate obj
                for object in objects! {
                    // storing infor in array
                    self.uuidArray.append(object.value(forKey: "uuid") as! String)
                    self.picArray.append(object.value(forKey: "pic") as! PFFileObject)
                }
                
                self.collectionView.reloadData()
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        // push back
        self.navigationController?.popViewController(animated: true)
        
        // clean guest username or deduct the last guest username from guestname - array
        if !guestName.isEmpty {
            guestName.removeLast()
        }
    }
    
    func configCollectionView() {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        // spacing of header and cell or footer
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        layout.itemSize = CGSize(width: Screen.witdh / 3, height: Screen.height / 3)
        
        // spacing each cell
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        // header always on top
        layout.sectionHeadersPinToVisibleBounds = true
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        // register cell
        collectionView.register(GuestPictureCell.nib(), forCellWithReuseIdentifier: GuestPictureCell.identifier)
        // register header
        collectionView.register(GuestHeaderView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: GuestHeaderView.identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
    }
    
}

extension GuestVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GuestPictureCell.identifier, for: indexPath) as! GuestPictureCell
        picArray[indexPath.row].getDataInBackground { (data, error) in
            if error == nil {
                cell.picImg.image = UIImage(data: data!)
            } else { print(error!.localizedDescription) }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Screen.witdh / 3, height: Screen.witdh / 3)
    }
    
    // header config
    fileprivate func infoGuest(_ header: GuestHeaderView) {
        // load data of guest
        let infoQuery = PFQuery(className: "_User")
        infoQuery.whereKey("username", equalTo: guestName.last!)
        infoQuery.findObjectsInBackground { (objects, error) in
            if error == nil {
                
                // shown wrong user
                if objects!.isEmpty {
                    print("wrong user")
                }
                // find related to user info
                for object in objects! {
                    header.textFieldFullname.text = (object.object(forKey: "fullname") as! String).uppercased()
                    header.textFieldBio.text = object.object(forKey: "bio") as? String
                    header.textFieldBio.sizeToFit()
                    header.textFieldWeb.text = object.object(forKey: "web") as? String
                    header.textFieldWeb.sizeToFit()
                    let avaFile: PFFileObject = (object.object(forKey: "ava") as? PFFileObject)!
                    avaFile.getDataInBackground { (data, error) in
                        if error == nil {
                            header.imgAvatar.image = UIImage(data: data!)
                        } else {
                            print(error!.localizedDescription)
                        }
                    }
                }
                
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func followBtnStatus(_ header: GuestHeaderView) {
        // show status current user follow guest or do not
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("follower", equalTo: PFUser.current()!.username!)
        followQuery.whereKey("following", equalTo: guestName.last!)
        followQuery.countObjectsInBackground { (count, error) in
            if error == nil {
                if count == 0 {
                    header.btnGuestFollow.setTitle("FOLLOW", for: UIControl.State.normal)
                    header.btnGuestFollow.backgroundColor = .systemBlue
                } else {
                    header.btnGuestFollow.setTitle("FOLLOWING", for: UIControl.State.normal)
                    header.btnGuestFollow.backgroundColor = .lightGray
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func totalPosts(_ header: GuestHeaderView) {
        // count statistics
        // count posts
        let posts = PFQuery(className: "posts")
        posts.whereKey("username", equalTo: guestName.last!)
        posts.countObjectsInBackground { (count, error) in
            if error == nil {
                header.posts.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func totalFollowers(_ header: GuestHeaderView) {
        // count statistics
        // count follower
        let followers = PFQuery(className: "follow")
        followers.whereKey("following", equalTo: guestName.last!)
        followers.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followers.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func totalFollowings(_ header: GuestHeaderView) {
        // count statistics
        // count following
        let following = PFQuery(className: "follow")
        following.whereKey("follower", equalTo: guestName.last!)
        following.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followings.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func tapAtPosts(_ header: GuestHeaderView) {
        // implement tap gestures
        // tap to posts
        let postsTap = UITapGestureRecognizer(target: self, action: #selector(postsTapping))
        postsTap.numberOfTapsRequired = 1
        header.posts.isUserInteractionEnabled = true
        header.posts.addGestureRecognizer(postsTap)
    }
    
    fileprivate func tapAtFollowers(_ header: GuestHeaderView) {
        // tap to followers
        let followersTap = UITapGestureRecognizer(target: self, action: #selector(followersTapping))
        followersTap.numberOfTapsRequired = 1
        header.followers.isUserInteractionEnabled = true
        header.followers.addGestureRecognizer(followersTap)
    }
    
    fileprivate func tapAtFollowings(_ header: GuestHeaderView) {
        // tap to followings
        
        let followingsTap = UITapGestureRecognizer(target: self, action: #selector(followingsTapping))
        followingsTap.numberOfTapsRequired = 1
        header.followings.isUserInteractionEnabled = true
        header.followings.addGestureRecognizer(followingsTap)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        // define header
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: GuestHeaderView.identifier, for: indexPath) as! GuestHeaderView
        
        infoGuest(header)
        followBtnStatus(header)
        totalPosts(header)
        totalFollowers(header)
        totalFollowings(header)
        
        // Implement tap gestures (when tap to posts, follower, following)
        tapAtPosts(header)
        tapAtFollowers(header)
        tapAtFollowings(header)
        
        
        return header
    }
    
    @objc func followingsTapping() {
        user = guestName.last!
        showing = "followings"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
    }
    
    @objc func followersTapping() {
        user = guestName.last!
        showing = "followers"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
        
    }
    
    @objc func postsTapping() {
        if !picArray.isEmpty {
            let index = IndexPath(item: 0, section: 0)
            self.collectionView.scrollToItem(at: index, at: UICollectionView.ScrollPosition.top, animated: true)
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.height / 3.2)
    }
    
    
}
