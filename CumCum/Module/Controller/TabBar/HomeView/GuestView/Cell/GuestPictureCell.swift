//
//  GuestPictureCell.swift
//  TEAM1
//
//  Created by net=0 on 17/10/20.
//

import UIKit

class GuestPictureCell: UICollectionViewCell {
    
    static let identifier = "GuestPictureCell"
    
    @IBOutlet weak var picImg: UIImageView!
    static func nib() -> UINib {
        return UINib(nibName: "GuestPictureCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
