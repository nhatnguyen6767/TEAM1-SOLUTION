//
//  GuestHeaderView.swift
//  TEAM1
//
//  Created by net=0 on 17/10/20.
//

import UIKit
import Parse

class GuestHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var textFieldFullname: UILabel!
    @IBOutlet weak var textFieldWeb: UITextView!
    @IBOutlet weak var textFieldBio: UILabel!
    @IBOutlet weak var posts: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followings: UILabel!
    @IBOutlet weak var titlePosts: UILabel!
    @IBOutlet weak var titleFollowers: UILabel!
    @IBOutlet weak var titleFollowings: UILabel!
    @IBOutlet weak var btnGuestFollow: UIButton!
    
    static let identifier = "GuestHeaderView"
    
    static func nib() -> UINib {
        return UINib(nibName: "GuestHeaderView", bundle: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func clickFollowBtn(_ sender: Any) {
        let title = btnGuestFollow.title(for: UIControl.State.normal)
        
        // to follow
        if title == "FOLLOW" {
            let object = PFObject(className: "follow")
            object["follower"] = PFUser.current()!.username
            object["following"] = guestName.last!
            object.saveInBackground { (success, error) in
                if error == nil {
                    self.btnGuestFollow.setTitle("FOLLOWING", for: UIControl.State.normal)
                    self.btnGuestFollow.backgroundColor = .lightGray
                } else {
                    print(error!.localizedDescription)
                }
            }
            // unfollow
        } else {
            let query = PFQuery(className: "follow")
            query.whereKey("follower", equalTo: PFUser.current()!.username!)
            query.whereKey("following", equalTo: guestName.last!)
            query.findObjectsInBackground { (objects, error) in
                if error == nil {
                    for object in objects! {
                        object.deleteInBackground { (success, error) in
                            if error == nil {
                                self.btnGuestFollow.setTitle("FOLLOW", for: UIControl.State.normal)
                                self.btnGuestFollow.backgroundColor = .systemBlue
                            } else {
                                print(error!.localizedDescription)
                            }
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
        }
    }
    
    
}
