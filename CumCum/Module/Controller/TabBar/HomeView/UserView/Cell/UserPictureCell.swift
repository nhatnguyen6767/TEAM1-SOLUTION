//
//  FourTabBarCollectionViewCell.swift
//  TEAM1
//
//  Created by net=0 on 11/10/20.
//

import UIKit

class UserPictureCell: UICollectionViewCell {
    
    
    @IBOutlet weak var picImg: UIImageView!
    
    static let identifier = "UserPictureCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "UserPictureCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
