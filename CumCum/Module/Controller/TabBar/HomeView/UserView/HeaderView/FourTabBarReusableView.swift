//
//  FourTabBarReusableView.swift
//  TEAM1
//
//  Created by net=0 on 12/10/20.
//

import UIKit

class HeaderView: UICollectionReusableView {

    static let identifier = "FourTabBarReusableView"
    
    static func nib() -> UINib {
        return UINib(nibName: "FourTabBarReusableView", bundle: nil)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
}
