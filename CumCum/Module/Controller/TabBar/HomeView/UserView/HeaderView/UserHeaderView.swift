//
//  FourTabBarReusableView.swift
//  TEAM1
//
//  Created by net=0 on 12/10/20.
//

import UIKit

class UserHeaderView: UICollectionReusableView {
    
    static let identifier = "UserHeaderView"
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var textFieldFullname: UILabel!
    @IBOutlet weak var textFieldWeb: UITextView!
    @IBOutlet weak var textFieldBio: UILabel!
    @IBOutlet weak var posts: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followings: UILabel!
    @IBOutlet weak var titlePosts: UILabel!
    @IBOutlet weak var titleFollowers: UILabel!
    @IBOutlet weak var titleFollowings: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    
    
    static func nib() -> UINib {
        return UINib(nibName: "UserHeaderView", bundle: nil)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        autoLayout()
    }
    
    func autoLayout() {
        let widthScreen = Screen.witdh
        let heightScreen = Screen.height
        
        // Initialization code
        imgAvatar.frame = CGRect(x: widthScreen / 16, y: widthScreen / 16, width: widthScreen / 4, height: widthScreen / 4)
        posts.frame = CGRect(x: widthScreen / 2.5, y: imgAvatar.frame.origin.y, width: widthScreen / 7, height: heightScreen / 22)
        followers.frame = CGRect(x: widthScreen / 1.6, y: imgAvatar.frame.origin.y, width: widthScreen / 7, height: heightScreen / 22)
        followings.frame = CGRect(x: widthScreen / 1.2, y: imgAvatar.frame.origin.y, width: widthScreen / 7, height: heightScreen / 22)
        
        titlePosts.center = CGPoint(x: posts.center.x, y: posts.center.y + 20)
        titleFollowers.center = CGPoint(x: followers.center.x, y: followers.center.y + 20)
        titleFollowings.center = CGPoint(x: followings.center.x, y: followings.center.y + 20)
        
        btnEditProfile.frame = CGRect(x: titlePosts.frame.origin.x, y: titlePosts.center.y + 20, width: widthScreen - titlePosts.frame.origin.x - 10, height: heightScreen / 22)
        textFieldFullname.frame = CGRect(x: imgAvatar.frame.origin.x, y: imgAvatar.frame.origin.y + imgAvatar.frame.size.height, width: widthScreen - 20, height: heightScreen / 22)
        textFieldWeb.frame = CGRect(x: imgAvatar.frame.origin.x - 5, y: textFieldFullname.frame.origin.y + 15, width: widthScreen - 20, height: heightScreen / 22)
        textFieldBio.frame = CGRect(x: imgAvatar.frame.origin.x, y: textFieldWeb.frame.origin.y + 30, width: widthScreen - 30, height: heightScreen / 22)
        
        // bound avatar
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
        imgAvatar.clipsToBounds = true
        
    }
    
    
    @IBAction func clickEditProfile(_ sender: Any) {
    }
    
}
