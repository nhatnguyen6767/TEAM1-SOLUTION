//
//  HomeVC.swift
//  CumCum
//
//  Created by net=0 on 11/10/20.
//

import UIKit
import Parse

class HomeVC: UIViewController {
    
    var refresher: UIRefreshControl!
    // how many pictures show, size of page
    var page: Int = 10
    
    var uuidArray = [String]()
    var picArray = [PFFileObject]()
    
    var collectionView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // header can not be scrolling
        self.collectionView?.alwaysBounceVertical = true
        
        // set user name in the top
        self.navigationItem.title = PFUser.current()?.username?.uppercased()
        self.navigationController?.navigationBar.barTintColor = .white
        let logoutBtn = UIBarButtonItem(image: UIImage(named: "logout")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(logout))
        navigationItem.rightBarButtonItem = logoutBtn
        
        configCollectionView()
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        collectionView?.addSubview(refresher)
        
        // load post
        loadPosts()
        
    }
    
    @objc func logout(){
        // the first show the dialog message
        Common.displayAlert(title: "Warning!!", message: "Are you sure you want to logout?", vc: self)
        
        PFUser.logOutInBackground { (error) in
            if error == nil {
                // remove logged in user from App memory
                UserDefaults.standard.removeObject(forKey: "username")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.login()
            } else {
                
            }
        }
    }
    
    @objc func refresh() {
        collectionView?.reloadData()
        refresher.endRefreshing()
    }
    
    func loadPosts() {
        
        let query = PFQuery(className: "posts")
        query.whereKey("username", contains: PFUser.current()!.username)
        query.limit = page
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                
                self.uuidArray.removeAll(keepingCapacity: false)
                self.picArray.removeAll(keepingCapacity: false)
                
                for object in objects! {
                    
                    // add found data to arrays
                    self.uuidArray.append(object.value(forKey: "uuid") as! String)
                    self.picArray.append(object.value(forKey: "pic") as! PFFileObject)
                }
                
                self.collectionView?.reloadData()
                
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.frame = view.bounds
    }
    
    func configCollectionView() {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        // space of header and cell or footer
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        layout.itemSize = CGSize(width: Screen.witdh / 3, height: Screen.witdh / 3)
        
        // spacing each cell
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
//        collectionView!.collectionViewLayout = layout
        // header alway on top
        layout.sectionHeadersPinToVisibleBounds = true
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        // register cell
        collectionView?.register(UserPictureCell.nib(), forCellWithReuseIdentifier: UserPictureCell.identifier)
        
        // register header
        collectionView?.register(UserHeaderView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: UserHeaderView.identifier)
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = .white
        view.addSubview(collectionView!)
        
        
    }


    

}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserPictureCell.identifier, for: indexPath) as! UserPictureCell
        picArray[indexPath.row].getDataInBackground { (data, error) in
            if error == nil {
                cell.picImg.image = UIImage(data: data!)
            } else {
                print(error!.localizedDescription)
            }
        }
        return cell
    }
    
    
    // define size of each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Screen.witdh / 3, height: Screen.witdh / 3)
    }
    
    
    // header
    fileprivate func totalPosts(_ header: UserHeaderView) {
        // couting posts
        let posts = PFQuery(className: "posts")
        posts.whereKey("username", contains: PFUser.current()!.username)
        posts.countObjectsInBackground { (count, error) in
            if error == nil {
                header.posts.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func infoUser(_ header: UserHeaderView) {
        header.textFieldFullname.text = (PFUser.current()!.object(forKey: "fullname") as! String).uppercased()
        header.textFieldWeb.text = PFUser.current()!.object(forKey: "web") as? String
        header.textFieldWeb.sizeToFit()
        header.textFieldBio.text = PFUser.current()?.object(forKey: "bio") as? String
        header.textFieldBio.sizeToFit()
        
        let avaQuery = PFUser.current()?.object(forKey: "ava") as! PFFileObject
        avaQuery.getDataInBackground { (data, error) in
            header.imgAvatar.image = UIImage(data: data!)
        }
        header.btnEditProfile.setTitle("edit profile", for: UIControl.State.normal)
    }
    
    fileprivate func totalFollowers(_ header: UserHeaderView) {
        // couting total followers
        let follower = PFQuery(className: "follow")
        follower.whereKey("following", contains: PFUser.current()!.username)
        follower.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followers.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func totalFollowings(_ header: UserHeaderView) {
        // couting total following
        let follower = PFQuery(className: "follow")
        follower.whereKey("follower", contains: PFUser.current()!.username)
        follower.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followings.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func tapAtPosts(_ header: UserHeaderView) {
        
        // tap posts
        let postsTap = UITapGestureRecognizer(target: self, action: #selector(postsTapping))
        postsTap.numberOfTapsRequired = 1
        header.posts.isUserInteractionEnabled = true
        header.posts.addGestureRecognizer(postsTap)
    }
    
    fileprivate func tapAtFollowers(_ header: UserHeaderView) {
        // follower tap
        let followersTap = UITapGestureRecognizer(target: self, action: #selector(followersTapping))
        followersTap.numberOfTapsRequired = 1
        header.followers.isUserInteractionEnabled = true
        header.followers.addGestureRecognizer(followersTap)
    }
    
    fileprivate func tapAtFollowings(_ header: UserHeaderView) {
        // following tap
        let followingsTap = UITapGestureRecognizer(target: self, action: #selector(followingsTapping))
        followingsTap.numberOfTapsRequired = 1
        header.followings.isUserInteractionEnabled = true
        header.followings.addGestureRecognizer(followingsTap)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: UserHeaderView.identifier, for: indexPath) as! UserHeaderView
        
        // set alway top of header
//        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        flow.sectionHeadersPinToVisibleBounds = true
        
        infoUser(header)
        totalPosts(header)
        totalFollowers(header)
        totalFollowings(header)
        
        // Implement tap gestures (when tap to posts, follower, following)
        tapAtPosts(header)
        tapAtFollowers(header)
        tapAtFollowings(header)
        
        

        return header
    }
    
    @objc func followingsTapping() {
        
        // the current user 
        user = PFUser.current()!.username!
        showing = "followings"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
        
    }
    
    @objc func followersTapping() {
        user = PFUser.current()!.username!
        showing = "followers"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
    }
    
    @objc func postsTapping() {
        if !picArray.isEmpty {
            let index = IndexPath(item: 0, section: 0)
            self.collectionView?.scrollToItem(at: index, at: UICollectionView.ScrollPosition.top, animated: true)
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.height / 3.2)
    }
    
    
}
