//
//  FollowersCell.swift
//  TEAM1
//
//  Created by net=0 on 15/10/20.
//

import UIKit
import Parse

class FollowersCell: UITableViewCell {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var textFieldUsername: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    
    static let identifier = "FollowersCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "FollowersCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        autoLayout()
        
        
    }
    
    func autoLayout() {
        
        let widthScreen = Screen.witdh
        let heightScreen = Screen.height
        
        imgAvatar.frame = CGRect(x: 10, y: 10, width: widthScreen / 5.3, height: widthScreen / 5.3)
        textFieldUsername.frame = CGRect(x: imgAvatar.frame.size.width + 20, y: 28, width: widthScreen / 3.2, height: heightScreen / 22)
        btnFollow.frame = CGRect(x: widthScreen - widthScreen / 3.5 - 10, y: 30, width: widthScreen / 3.5, height: heightScreen / 22)
        btnFollow.layer.cornerRadius = btnFollow.frame.size.width / 20
        
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2
        imgAvatar.clipsToBounds = true
        
    }
    
    
    @IBAction func clickFollowBtn(_ sender: Any) {
        let title = btnFollow.title(for: UIControl.State.normal)
        
        // to follow
        if title == "FOLLOW" {
            let object = PFObject(className: "follow")
            object["follower"] = PFUser.current()!.username
            object["following"] = textFieldUsername.text
            object.saveInBackground { (success, error) in
                if error == nil {
                    self.btnFollow.setTitle("FOLLOWING", for: UIControl.State.normal)
                    self.btnFollow.backgroundColor = .lightGray
                } else {
                    print(error!.localizedDescription)
                }
            }
            // unfollow
        } else {
            let query = PFQuery(className: "follow")
            query.whereKey("follower", equalTo: PFUser.current()!.username!)
            query.whereKey("following", equalTo: textFieldUsername.text!)
            query.findObjectsInBackground { (objects, error) in
                if error == nil {
                    for object in objects! {
                        object.deleteInBackground { (success, error) in
                            if error == nil {
                                self.btnFollow.setTitle("FOLLOW", for: UIControl.State.normal)
                                self.btnFollow.backgroundColor = .systemBlue
                            } else {
                                print(error!.localizedDescription)
                            }
                        }
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
        }
    }
    
    
}
