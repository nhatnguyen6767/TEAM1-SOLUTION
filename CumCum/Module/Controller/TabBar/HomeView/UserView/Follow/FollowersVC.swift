//
//  FollowersVC.swift
//  TEAM1
//
//  Created by net=0 on 15/10/20.
//

import UIKit
import Parse

var showing = String()
var user = String()

class FollowersVC: UITableViewController {
    
    // be holding data received from servers
    var usernameArray = [String]()
    var avaArray = [PFFileObject]()
    
    // showing who do we follow
    var followArray = [String]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(FollowersCell.nib(), forCellReuseIdentifier: FollowersCell.identifier)
        
        self.navigationItem.title = showing.uppercased()
        
        if showing == "followers" {
            loadFollowers()
        } else if showing == "followings" {
            loadFollowings()
        }
        
        

        
    }

    func loadFollowers() {
        
        // find followers of user
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("following", equalTo: user)
        followQuery.findObjectsInBackground { (objects, error) in
            if error == nil {
                // clean up
                self.followArray.removeAll(keepingCapacity: false)
                
                // find relate obj depend on query result
                for object in objects! {
                    self.followArray.append(object.value(forKey: "follower") as! String)
                }
                
                // find users following user - (twice solution)
//                let query = PFQuery(className: "_User")
                let query = PFUser.query()
                query?.whereKey("username", containedIn: self.followArray)
                query?.addDescendingOrder("createdAt")
                query?.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        // clean up
                        self.usernameArray.removeAll(keepingCapacity: false)
                        self.avaArray.removeAll(keepingCapacity: false)
                        
                        // find relate obj in User class of DB
                        for object in objects! {
                            self.usernameArray.append(object.object(forKey: "username") as! String)
                            self.avaArray.append(object.object(forKey: "ava") as! PFFileObject)
                            self.tableView.reloadData()
                        }
                        
                    } else {
                        print(error!.localizedDescription)
                    }
                })
                
            } else {
                print(error!.localizedDescription)
            }
        }
        
    }
    
    func loadFollowings() {
        
        let followQuery = PFQuery(className: "follow")
        followQuery.whereKey("follower", equalTo: user)
        followQuery.findObjectsInBackground { (objects, error) in
            
            if error == nil {
                
                // clean up
                self.followArray.removeAll(keepingCapacity: false)
                
                // find relate obj in "follow" of DB
                for object in objects! {
                    self.followArray.append(object.value(forKey: "following") as! String)
                }
                
                // find users followed by user
                let query = PFQuery(className: "_User")
                query.whereKey("username", containedIn: self.followArray)
                query.addDescendingOrder("createdAt")
                query.findObjectsInBackground { (objects, error) in
                    
                    if error == nil {
                        
                        // clean up
                        self.usernameArray.removeAll(keepingCapacity: false)
                        self.avaArray.removeAll(keepingCapacity: false)
                        
                        for object in objects! {
                            self.usernameArray.append(object.object(forKey: "username") as! String)
                            self.avaArray.append(object.object(forKey: "ava") as! PFFileObject)
                            self.tableView.reloadData()
                        }
                        
                    } else {
                        print(error!.localizedDescription)
                    }
                    
                }
                
            } else {
                print(error!.localizedDescription)
            }
            
        }
    }
    
    // MARK: - Table view data source

    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return usernameArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // define cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersCell", for: indexPath) as! FollowersCell
        cell.textFieldUsername.text = usernameArray[indexPath.row]
        avaArray[indexPath.row].getDataInBackground { (data, error) in
            if error == nil {
                cell.imgAvatar.image = UIImage(data: data!)
            } else {
                print(error!.localizedDescription)
            }
        }
        
        // show user following or do not
        let query = PFQuery(className: "follow")
        query.whereKey("follower", equalTo: PFUser.current()!.username)
        query.whereKey("following", equalTo: cell.textFieldUsername.text!)
        query.countObjectsInBackground { (count, error) in
            if error == nil {
                if count == 0 {
                    cell.btnFollow.setTitle("FOLLOW", for: UIControl.State.normal)
                    cell.btnFollow.backgroundColor = .systemBlue
                } else {
                    cell.btnFollow.setTitle("FOLLOWING", for: UIControl.State.normal)
                    cell.btnFollow.backgroundColor = .lightGray
                }
            } else {
                print(error!.localizedDescription)
            }
        }
        
        // hide follow button if the current user
        if cell.textFieldUsername.text == PFUser.current()?.username {
            cell.btnFollow.isHidden = true
        }
        
        return cell
    }
    
    // when tapping at row of follower or following
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FollowersCell
        
        // if user tapped on himself, go home, else go guest
        if cell.textFieldUsername.text! == PFUser.current()!.username! {
            self.navigationController?.pushViewController(HomeVC(), animated: true)
        } else {
            guestName.append(cell.textFieldUsername.text!)
            self.navigationController?.pushViewController(GuestVC(), animated: true)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return Screen.height / 10
        return Screen.witdh / 4
    }

    
}
