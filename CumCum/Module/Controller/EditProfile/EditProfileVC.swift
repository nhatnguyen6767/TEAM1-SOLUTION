//
//  EditProfileVC.swift
//  CumCum
//
//  Created by bbineh on 13/11/20.
//

import UIKit

class EditProfileVC: UIViewController {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldWeb: UITextField!
    @IBOutlet weak var textFieldBio: UITextView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldTel: UITextField!
    @IBOutlet weak var textFieldGender: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configNavItem()

    }
    
    func configNavItem() {
        self.navigationController?.navigationBar.barTintColor = .white
        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction))
        navigationItem.leftBarButtonItem = cancelBtn
        let saveBtn = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveAction))
        navigationItem.rightBarButtonItem = saveBtn
    }
    
    @objc func saveAction() {
        print("Saving....")
    }
    
    @objc func cancelAction() {
        print("Cancel....")
    }

}


/**
set user name in the top
 self.navigationItem.title = PFUser.current()?.username?.uppercased()
 self.navigationController?.navigationBar.barTintColor = .white
 let logoutBtn = UIBarButtonItem(image: UIImage(named: "logout")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(logout))
 navigationItem.rightBarButtonItem = logoutBtn
 */
