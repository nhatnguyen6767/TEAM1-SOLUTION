//
//  ForgotPasswordVC.swift
//  TEAM1
//
//  Created by net=0 on 11/10/20.
//

import UIKit
import Parse

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var btnReset: CornerItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reset Password"

        hideKeyboardWhenTap()
        
    }
    
    func hideKeyboardWhenTap(){
        //Looks for single or multiple taps.
        let hideTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        view.addGestureRecognizer(hideTap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @IBAction func btnResetClick(_ sender: Any) {
        dismissKeyboard()
        let userEmail = textFieldEmail.text
        if userEmail!.isEmpty {
            Common.displayAlert(title: "Email field", message: "is empty", vc: self)
        }
        PFUser.requestPasswordResetForEmail(inBackground: userEmail!)
        Common.displayAlert(title: "Success", message: "send message to " + userEmail!, vc: self)
        self.navigationController?.popViewController(animated: true)
    }
    
}
