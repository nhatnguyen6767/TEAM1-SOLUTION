//
//  LoginVC.swift
//  NetworkingTeam
//
//  Created by net=0 on 9/10/20.
//

import UIKit
import FBSDKLoginKit
import Parse

class SignInVC: UIViewController {
    
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    
    var isLoginFBSuccess = false
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // To set new font for name of product
        //        usernameTxt.font = UIFont(name: "Pacifico", size: 25)
        
        self.navigationController?.isNavigationBarHidden = true
        hideKeyboardWhenTap()
        
        
        // after check login fb go to mainvc
        //        isUserValid()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        //        isUserValid()
        
    }
    
    func isUserValid(){
        
        // when open app, check token of user, if have go to mainvc
        if (AccessToken.current != nil && isLoginFBSuccess == true) {
            FBManager.getFBUserData {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.tabBarControllerView()
                
            }
        } else {
            
        }
        
    }
    
    func hideKeyboardWhenTap(){
        //Looks for single or multiple taps.
        let hideTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        view.addGestureRecognizer(hideTap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @IBAction func clickSignInBtn(_ sender: Any) {
        dismissKeyboard()
        
        guard let email = textFieldUsername.text else {
            Common.displayAlert(title: "Please", message: "fill all fields", vc: self)
            return
        }
        guard let password = textFieldPassword.text else {
            Common.displayAlert(title: "Please", message: "fill all fields", vc: self)
            return
        }
        
//        if textFieldUsername.text!.isEmpty || textFieldPassword.text!.isEmpty {
//            Common.displayAlert(title: "Please", message: "fill all fields", vc: self)
//        }
        // login
        PFUser.logInWithUsername(inBackground: textFieldUsername.text!, password: textFieldPassword.text!) { (user, error) in
            if error == nil {
                UserDefaults.standard.set(user!.username, forKey: "username")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.login()
                
            } else {
                let systemErr = error!.localizedDescription
                Common.displayAlert(title: "ERROR!!!", message: systemErr, vc: self)
            }
        }
        
    }
    
    
    @IBAction func clickBtnFacebook(_ sender: Any) {
        dismissKeyboard()
        
        if (AccessToken.current != nil) {
            
            
            self.isLoginFBSuccess = true
            self.viewDidAppear(true)
            
            
        } else {
            FBManager.shared.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
                
                if (error == nil) {
                    FBManager.getFBUserData {
                        
                        self.isLoginFBSuccess = true
                        self.viewDidAppear(true)
                        
                    }
                    
                } else {
                    print(error!.localizedDescription)
                }
                
            }
        }
        
        
    }
    
    func saveFacebookAccount(){
        
    }
    
    
    
    
    @IBAction func clickBtnSignUp(_ sender: Any) {
        self.navigationController?.pushViewController(SignUpVC(), animated: true)
        
    }
    
    
    @IBAction func clickBtnForgot(_ sender: Any) {
        self.navigationController?.pushViewController(ForgotPasswordVC(), animated: true)
    }
    
    
}
